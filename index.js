// Array Methods
// JavaScript has built in functions and methods for arrays. This allows us to manipulate and access array elements.

// [Section] Mutator Methods
// Mutator methods are functions that mutate or change an array after they are created.
// These methods manipulate the original array performing various tasks such as adding and removing elements.

let fruits = ["Apple","Orange","Kiwi","Dragon Fruit"];
console.log(fruits)


	// push()
		/*
			-Adds an element in the end of an array and returns the array's length

			Syntax:
				arrayName.push(elementsToBeAdded);
		*/

		console.log("Current Array Fruits []:");
		console.log(fruits);

		let fruitsLength = fruits.push("Mango");
		/*fruits.push("Mango");*/
		console.log("Mutated Array from PUSH Method: ");
		console.log(fruits);
		console.log(fruitsLength);

		// Adding multiple elements to an array
		fruitsLength = fruits.push("Avocado","Guava");
		console.log("Mutated Array from PUSH Method: ");
		console.log(fruits);
		console.log(fruitsLength);


	// pop()
		/*
			- Removes the last element in an array and returns the removed element.

			Syntax:
				arrayName.pop();
		*/

		console.log("Current Array Fruits[]: ");
		console.log(fruits);

		let removedFruit = fruits.pop();
		/*fruits.pop();*/
		console.log("Mutated Array from POP Method: ");
		console.log(fruits);
		console.log(removedFruit);

		console.log("Current Array Fruits[]: ");
		removedFruit = fruits.pop();
		/*fruits.pop();*/
		console.log("Mutated Array from POP Method: ");
		console.log(fruits);
		console.log(removedFruit);


	// unshift()
		/*
			- Adds one or more elements at the beginning of an array and returns the present length

			Syntax:
				arrayName.unshift('elementA');
				arrayName.unshift('elementA', 'element B', ...);
		*/

		console.log("Current Array Fruits[]: ")
		console.log(fruits)

		fruitsLength = fruits.unshift('Lime', 'Banana')
		/*fruits.unshift('Lime', 'Banana')*/
		console.log("Mutated Array After the UNSHIFT Method: ")
		console.log(fruits);
		console.log(fruitsLength);

	// shift()
		/*
			- Removes an element at the beginning of an array
			- It also returns the removed element.

			Syntax:
				arrayName.shift();
		*/

		console.log("Current Array Fruits[]: ")
		console.log(fruits)

		removedFruit = fruits.shift()
		/*fruits.shift()*/
		console.log("Mutated Array After the SHIFT Method: ")
		console.log(fruits);
		console.log(removedFruit);


	// splice()
		/*
			-Simultataniously removes elements from a specified index number and adds elements

			Syntax:
				arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
		*/

		console.log("Current Array Fruits[]: ")
		console.log(fruits)

		fruits.splice(1, 1, "Lime")
		console.log("Mutated Array After the SPLICE Method: ")
		console.log(fruits);

		let index = 3
		console.log(fruits);
		console.log(fruits.splice(3, 1)); /*or (index, 1)*/
		/*fruits.splice(5, 0, "Cherry")*/
		console.log(fruits)

		console.log("Current Array Fruits[]: ")
		console.log(fruits)

		console.log(fruits.splice(3, 0, "Durian","Santol"))
		console.log("Mutated Array After the SPLICE Method: ")
		console.log(fruits);


	// sort()
		/*
			- Rearranges the array elements in alphanumeric order

			Important Note:
			- The sort method is used for more complicated functions
			- It focused the bootcampers on the basic usage of the sort method

			Syntax:
				arrayName.sort()
		*/

		console.log("Current Array Fruits[]: ")
		console.log(fruits)
		
		console.log(fruits.sort())
		console.log("Mutated Array After the SORT Method: ")
		console.log(fruits);


	// reverse()
		/*
			- It reverses the order of array elements

			Syntax:
				arrayName.reverse()
		*/

		console.log("Current Array Fruits[]: ")
		console.log(fruits)
		console.log("Return of reverse() method")
		console.log(fruits.reverse())

		console.log("Mutated Array After the REVERSE Method: ")
		console.log(fruits);



// [Section] Non-Mutator Methods
// they are the functions that do not modify or change an array after they are created
// These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.

let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE", "PH"]

	// indexOf()
		/*
			- It returns the index numebr of the first matching element found in an array
			- If no match was found, the result will be -1
			- The search process will be done from first element proceeding to the last element

			Syntax:
				arrayName.indexOf(searchValue)
				arrayName.indexOf(searchValue, staringIndex)
		*/

		console.log(countries.indexOf('PH')) 
		console.log(countries.indexOf('BR')) /*ans: -1*/

		/*Example of arrayName.indexOf(searchValue, staringIndex)*/
		console.log(countries.indexOf('PH', 2))


	// lastIndexOf()
		/*
			- It returns the index number of the last element of last matching element found in an array
			- The search process will be odne from last element proceeding to the first element

			Syntax:
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, startingIndex);
		*/

		console.log(countries.lastIndexOf('PH'))


	// slice()
		/*
			- Portion slices from an array and returns a new array

			Syntax:
				arrayName.slice(startingIndex)
				arrayName.slice(startingIndex, endingIndex)
		*/

		// Slicing off elements from a specified index to the last element
		let slicedArrayA = countries.slice(2)
		console.log(slicedArrayA)
		console.log(countries)

		// Slicing off elements from a specified index to another index
		let slicedArrayB = countries.slice(1,5)
		console.log(slicedArrayB)

		// Slicing off elements starting from the last element of an array
		let  slicedArrayC = countries.slice(-3, -1)
		console.log(slicedArrayC)


	// toString()
		/*
			-Returns an array as astring separated by commas

			Syntax:
				arrayName.toString()
		*/

		let stringedArray = countries.toString()
		console.log(stringedArray)


	// concat()
		/*
			- Combines arrays and returns the combined result

			Syntax:
				arrayA.concat(arrayB);
				arrayA.concat(elementA);
		*/

		let taskArrayA = ["drink html", "eat javascript"]
		let taskArrayB = ["inhale css", "breathe sass"]
		let taskArrayC = ["get git", "be node"]

		let tasks = taskArrayA.concat(taskArrayB)
		console.log("Result from CONCAT Method: ")
		console.log(tasks)

		// Combining multiple arrays
		console.log("Result from CONCAT Method: ")
		let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
		console.log(allTasks)

		// Combining arrays with elements
		let combinedTasks = taskArrayA.concat("smell express", "throw react")
		console.log(combinedTasks)

	// join()
		/*
			- Retuns an array as string separated by specified separator string

			Syntax:
				arrayName.join('seaparatorString')
		*/

		let users = ['John','Jane','Joe','Robert']
		console.log(users.join())
		console.log(users.join(' '))
		console.log(users.join(' - '))



// [Section] Iteration Methods
/*
	-Iteration methods are loop designed to perform repetitive tasks in arrays
	-Iteration method loops over all elements in array

	Syntax:
		arrayName.forEach(function(individualElement){
			statement/s;
		})
*/


	// forEach()
		/*
			-Similar to for loop that iterates on each of array element
			-For each element in the array, the function in the forEach method will run
		*/

		console.log(allTasks)
		allTasks.forEach(function(task){
			console.log(task)
		})

		let filteredTasks = []
		
		allTasks.forEach(function(task){
			if(task.length > 10){
				filteredTasks.push(task);
			}
		})

		console.log(filteredTasks)

	// map()
		/*
			-Iterates on each element and returns new array with different values depending on the result of the function's operation

			Syntax:
				let/const resultArray = arrayName.map(function(elements){
					statement/s;
					return;
				})
		*/

		let numbers = [1, 2, 3, 4, 5];

		let numberMap = numbers.map(function(number){
			return number*number
		})

		console.log("Original Array: ")
		console.log(numbers);
		console.log("Result of MAP Method:")
		console.log(numberMap)

	// every()
		/*
			-Checks if all elements in an array meet in the given condition
			-This is useful validaing data stored in arrays, especially when dealing with large amounts of data
			-It will return a true value if all elements meet the condition and false if otherwise

			Syntax:
				let/const resultArray = arrayName.every(function(element){
					return expression/condition;
				})
		*/

		console.log(numbers);
		let allValid = numbers.every(function(number){
			return (number < 6); /*true*/
			/*return (number < 3); /*false*/
		})

		console.log(allValid)

	// some()
		/*
			-It checks if at least one element in the array meets the given condition
			-It returns a true value if at least one element meets the condition and false if non

			Syntax:
				let/const resultArray = arrayName.some(function(elements){
					return expression/condition 
				})
		*/

		console.log(numbers);
		let someValid = numbers.some(function(number){
			return (number > 5) /*false*/
			/*return (number < 2) /*true*/
		})

		console.log(someValid)

	// filter()
		/*
			-It will retun new array that contains the elements which meets the given condition
			-It will retun an empty array if no element/s were found

			Syntax:
				let/const resultArray = arrayName.filter(function(element){
					return expression/condition
				})
		*/

		console.log(numbers);
		let filterValid = numbers.filter(function(number){
			return (number < 3);
			return (number === 0); /*empty*/
		})

		console.log(filterValid)

	// includes()
		/*
			-It checks if the argument passed can be found in the array
			-Case sensitive
			-It retruns boolean which can be saved in variable
			-True - if the argument is found in the array
			-False - if it is not

			Syntax:
				arrayName.includes(argument)
		*/

		let products = ['mouse','keyboard', 'laptop', 'monitor']

		let productFound1 = products.includes('mouse')
		console.log(productFound1) /*true*/

		let productFound2 = products.includes('headset')
		console.log(productFound2) /*false*/

	// reduce()
		/*
			-It will evaluate elements from left to right and returns or reduces the array into a single value

			Syntax:
				let/const resultValue = arrayName.reduce(function(accumulator, currentValue)){
					return expression/operation
				})
		*/

			console.log(numbers)
			let total = numbers.reduce(function(x,y){
				console.log("This is the value of x: " + x)
				console.log("This is the value of y: " + y)
				return x + y;
			})

			console.log(total);